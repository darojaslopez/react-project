import React from 'react';
import { useContext } from 'react';
import { useMediaQuery } from 'react-responsive';
import './styles/App.css';
import './styles/navbar.css';
import { Route, Routes, Link, Navigate } from 'react-router-dom';
import { Private } from './pages/Private';
import { Fruta } from './pages/Fruta';
import { RequireAuth } from './contexts/Auth/RequireAuth';
import { AuthContext } from './contexts/Auth/AuthContext';
import { Login } from './pages/Login';

function App() {
  const auth = useContext(AuthContext);

  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)'
  });

  const handleLogout = async () => {
    await auth.signout();
    window.location.href = '/login';
  };

  return (
    <div className={`App ${isDesktopOrLaptop ? 'desktop' : 'mobile'}`}>
      <header>
        <div className="navbar">
          <div className="title">Prueba</div>
          {auth.user && (
            <div className="nav-links">
              <Link to="/private" className="nav-link">Página Privada</Link>
              <Link to="/fruta" className="nav-link">Fruta</Link>
              <button className="logout-button" onClick={handleLogout}>Salir</button>
            </div>
          )}
        </div>
      </header>
      <hr />
      <Routes>
        <Route path="/" element={<Navigate to="/login" />} />
        <Route path="/login" element={<Login />} />
        {auth.user ? (
          <>
            <Route
              path="/private"
              element={<RequireAuth><Private /></RequireAuth>}
            />
            <Route
              path="/fruta"
              element={<RequireAuth><Fruta /></RequireAuth>}
            />
          </>
        ) : null}
      </Routes>
    </div>
  );
}

export default App;
