import React, { ChangeEvent, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../contexts/Auth/AuthContext";

export const Login = () => {
  const auth = useContext(AuthContext);
  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleEmailInput = (event: ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  }

  const handlePasswordInput = (event: ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  }

  const handleLogin = async () => {
    if (email && password) {
      const isLogged = await auth.signin(email, password);
      if (isLogged) {
        navigate('/private');
      } else {
        alert("No");
      }
    }
  }

  return (
    <div className="login-container login-container-center">
      <h2>Login</h2>

      <p style={{ color: '#666', fontStyle: 'italic' }}>
        Use las siguientes credenciales para iniciar sesión: prueba@gmail.com / contraseña123
      </p>

      <form className="login-form">
        <div className="form-group">
          <label>Email:</label>
          <input
            type="text"
            value={email}
            onChange={handleEmailInput}
            placeholder="Enter your email"
          />
        </div>

        <div className="form-group">
          <label>Password:</label>
          <input
            type="password"
            value={password}
            onChange={handlePasswordInput}
            placeholder="Enter your password"
          />
        </div>

        <button className="login-button" onClick={handleLogin}>Login</button>
      </form>
    </div>
  );
};
