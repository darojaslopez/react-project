import React, { useContext } from 'react';
import { AuthContext } from '../../contexts/Auth/AuthContext';
import { Login } from './../Login'; // Importa tu componente de login

export const Home = () => {
  const auth = useContext(AuthContext);

  return (
    <div>
      {auth.user ? (
        <p>¡Ya estás autenticado!</p>
      ) : (
        <Login />
      )}
    </div>
  );
};
