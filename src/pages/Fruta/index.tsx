import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../contexts/Auth/AuthContext";
import "./../../styles/fruta.css";
import DetalleFruta from './DetalleFruta';

type Fruta = {
  id: number;
  nombre: string;
  color: string;
  valor: string;
};

const FrutaItem: React.FC<Fruta & { onSelect: () => void }> = ({ id, nombre, color, valor, onSelect }) => (
  <div className="fruta-item" onClick={onSelect}>
    <strong>ID:</strong> {id} - <strong>Nombre:</strong> {nombre}
  </div>
);

export const Fruta = () => {
  const auth = useContext(AuthContext);
  const [frutas, setFrutas] = useState<Fruta[]>([]);
  const [loading, setLoading] = useState(true);
  const [selectedFruta, setSelectedFruta] = useState<Fruta | null>(null);

  useEffect(() => {
    const obtenerFrutas = async () => {
      try {
        const response = await fetch("https://apimocha.com/pruebareact/frutas");

        if (response.status === 200) {
          const data = await response.json();

          console.log("Respuesta de la API:", data);

          if (Array.isArray(data)) {
            setFrutas(data);
            setLoading(false);
          } else {
            console.error("La respuesta de la API no es un array:", data);
            setLoading(false);
          }
        } else {
          console.error("Error en la respuesta de la API:", response.status);
          setLoading(false);
        }
      } catch (error) {
        console.error("Error al obtener frutas:", error);
        setLoading(false);
      }
    };

    obtenerFrutas();
  }, []);

  const handleFrutaClick = (fruta: Fruta) => {
    setSelectedFruta(fruta);
  };

  return (
    <div className="fruta-container">
      <h2>Listado de Frutas Disponibles</h2>
      {loading ? (
        <p>Cargando...</p>
      ) : frutas.length > 0 ? (
        <div className="frutas-list">
          {frutas.map((fruta) => (
            <FrutaItem key={fruta.id} {...fruta} onSelect={() => handleFrutaClick(fruta)} />
          ))}
        </div>
      ) : (
        <p>No hay frutas disponibles.</p>
      )}

      {selectedFruta && (
        <DetalleFruta fruta={selectedFruta} />
      )}
    </div>
  );
};