import React from 'react';
import { Link } from 'react-router-dom';

type Fruta = {
  id: number;
  nombre: string;
  color: string;
  valor: string;
};

type DetalleFrutaProps = {
  fruta: Fruta;
};

const DetalleFruta: React.FC<DetalleFrutaProps> = ({ fruta }) => (
  <div className="detalle-fruta">
    <h3>Detalles de la fruta</h3>
    <p>Nombre: {fruta.nombre}</p>
    <p>Color: {fruta.color}</p>
    <p>Valor: {fruta.valor}</p>
  </div>
);

export default DetalleFruta;