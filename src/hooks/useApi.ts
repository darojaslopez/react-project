import axios from 'axios';

export const useApi = () => ({
    validateToken: async (token: string) => {
        const tokenToValidate = '123456789';
        if (token === tokenToValidate) {
            return {
                user: { id: 3, name: 'prueba', email: 'prueba@gmail.com' }
            };
        } else {
            alert('Token inválido');
            throw new Error('Token inválido');
        }
    },
    signin: async (email: string, password: string) => {
        if (email === 'prueba@gmail.com' && password === 'contraseña123') {
            return {
                user: { id: 3, name: 'prueba', email: 'prueba@gmail.com' },
                token: '123456789'
            };
        } else {
            alert('Credenciales inválidas');
            throw new Error('Credenciales inválidas');
        }
    },
    logout: async () => {
        console.log('Usuario deslogueado');
    }
});
